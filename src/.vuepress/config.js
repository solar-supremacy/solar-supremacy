const { description } = require('../../package')

module.exports = {
  base: "/solar-supremacy/",
  dest: "public",
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#title
   */
  title: 'Solar S. Project Site',
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#description
   */
  description: description,

  

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ['meta', { name: 'theme-color', content: '#9a3eaf' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }]
  ],

  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    repo: '',
    editLinks: false,
    docsDir: '',
    editLinkText: '',
    lastUpdated: false,
    nav: [
      {
        text: 'Test1',
        link: '/guide/',
      },
      {
        text: 'Test2',
        link: '/config/'
      },
      {
        text: 'Youtube',
        link: 'https://www.youtube.com/channel/UC4rMIkzGkPBN7iv4YBIJYjQ'
      }
    ],
    sidebar: {
      '/guide/': [
        {
          title: 'Guide',
          collapsable: false,
          children: [
            '',
            'using-vue',
          ]
        }
      ],
    }
    
  },

  /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: [
    '@vuepress/plugin-back-to-top',
    '@vuepress/plugin-medium-zoom',
  ]
}
